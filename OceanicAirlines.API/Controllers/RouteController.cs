﻿using System;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using OceanicAirlinesCORE.Implementations;
using OceanicAirlinesCORE.Interfaces;
using OceanicAirlinesCORE.Models;
using System.Web.Http.Cors;
using Microsoft.AspNetCore.Http.Internal;

namespace OceanicAirlines.API.Controllers
{
    [Route("api/[controller]")]
    public class RouteController : Controller
    {
        private IRouteService _routeService;

        public RouteController()
        {_routeService = new RouteService();

        }

        // GET: api/<controller>
        [HttpGet]
        public IActionResult Get([FromQuery] string fromName, 
            [FromQuery] string toName, 
            [FromQuery] string weight, 
            [FromQuery] int filter, 
            [FromQuery] string parcelType)
        {
            WeightTypes wType;
            switch (weight)
            {
                case "lessthan1kg":
                    wType = WeightTypes.Small;
                    break;
                case "1-5kg":
                    wType = WeightTypes.Medium;
                    break;
                case "morethan5kg":
                    wType = WeightTypes.Small;
                   break;
                default:
                    return BadRequest();
            }

            try
            {
                _routeService.GetResult(fromName, toName, wType, parcelType, filter);
            }
            catch (Exception e)
            {
                BadRequest(new ReturnDetails
                {
                    valid = false,
                    duration = 0,
                    price = 0,

                });
            }


            ReturnDetails result = new ReturnDetails()
            {
                valid = true,
                duration = 8,
                price = 100,
                fromName = "Congo",
                toName = "Cairo",
            };


            //if discount apply
            return Ok(result);
        }

    }

}
