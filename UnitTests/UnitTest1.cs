using NUnit.Framework;
using OceanicAirlinesCORE.Implementations;
using OceanicAirlinesCORE.Models;
using System.Collections.Generic;
using System.Diagnostics;

namespace UnitTests
{
    [TestFixture]
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void Test1()
        {
            List<Edge> edges = DataService.GetEdges();
            Assert.That(edges.Count.Equals(23));
            Assert.Pass();
        }
    }
}