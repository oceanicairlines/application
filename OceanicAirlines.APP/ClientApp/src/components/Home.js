import React, { Component } from 'react';
import { Button, Card, CardText, CardBody,
  CardTitle, Row, Col, ButtonDropdown, DropdownToggle, DropdownMenu, DropdownItem, ButtonGroup, Table} from 'reactstrap';
import "./Home.css";
  



export class Home extends Component {
  static displayName = Home.name;

  constructor(props) {
    super(props);
    this.toggleFrom = this.toggleFrom.bind(this);
    this.toggleTo = this.toggleTo.bind(this);
    this.toggleWeight = this.toggleWeight.bind(this);
    this.toggleParcelType = this.toggleParcelType.bind(this);
    
    this.state = {
      dropdownOpenFrom: false,
      dropdownOpenTo: false,
      dropdownOpenWeight: false,
      dropdownParcelType: false,
      fromValue: "Select start destination",
      toValue: "Select to destination",
      weightValue: "Select Weight",
      parcelTypeValue: "Select Parcel Type",
      rSelected: "Minimize Delivery Time",
      finalPrice: null,
      finalDuration: null,
    };

    this.onSearchClick = this.onSearchClick.bind(this);
    this.onRadioBtnClick = this.onRadioBtnClick.bind(this);
    this.onClickFrom = this.onClickFrom.bind(this);
    this.onClickTo = this.onClickTo.bind(this);
    this.onClickWeight = this.onClickWeight.bind(this);
    this.onClickParcelType = this.onClickParcelType.bind(this);
  }

  onRadioBtnClick(rSelected) {
    this.setState({ rSelected });
  }

  toggleFrom() {
    this.setState({
      dropdownFrom: !this.state.dropdownFrom
    });
  }

  toggleTo() {
    this.setState({
      dropdownTo: !this.state.dropdownTo
    });
  }

  toggleWeight() {
    this.setState({
      dropdownWeight: !this.state.dropdownWeight
    });
  }

  toggleParcelType() {
    this.setState({
      dropdownParcelType: !this.state.dropdownParcelType
    });
  }

  onClickFrom(e) {
    this.setState({
      fromValue:  e.currentTarget.textContent
    })
  }

  onClickTo(e) {
    this.setState({
      toValue:  e.currentTarget.textContent
    })
  }

  onClickWeight(e) {
    this.setState({
      weightValue:  e.currentTarget.textContent
    })
  }

  onClickParcelType(e) {
    this.setState({
      parcelTypeValue:  e.currentTarget.textContent
    })
  }

  onSearchClick()
  {
    var filter = "Price";
    if(this.state.radioSelected === "Minimize Delivery Time") filter = "Duration";
    const http = new XMLHttpRequest();
    var url = `http://wa-oadk.azurewebsites.net/api/route?fromName=${this.state.fromValue}&toName=${this.state.toValue}&weight=${this.state.weightValue}&filter=${filter}&parcelType=${this.state.parcelTypeValue}`;
    url = url.replace(/\s/g,'')
    http.open("GET", url);
    http.send();

    http.onreadystatechange=(e)=>
    {
      if(http.status != 200)
      {
        this.setState({finalPrice: "0"});
        this.setState({finalDuration: "0"});
      }
      else
      {

        console.log(http.responseType)
        var x = http.response;
        this.setState({finalPrice: http.response.price});
        this.setState({finalDuration: http.response.duration})
      }
    }
  }

  onRadioBtnClick(rSelected) {
    this.setState({ rSelected });
  }


  render () {
    return (
      <div>
        <h1>Get it there first!</h1>
        <main>
          <Row>
            <Col sm="4">
              <Card>
                <CardBody>
                  <CardTitle>Start Destination</CardTitle>
                  <CardText>
                    <ButtonDropdown isOpen={this.state.dropdownFrom} toggle={this.toggleFrom}>
                    <DropdownToggle caret>
                      {this.state.fromValue}
                    </DropdownToggle>
                      <DropdownMenu>
                        <DropdownItem onClick={this.onClickFrom}>HVALBUGTEN</DropdownItem>
                        <DropdownItem onClick={this.onClickFrom}>KAPSTADEN</DropdownItem>
                        <DropdownItem onClick={this.onClickFrom}>MOZAMBIQUE</DropdownItem>
                        <DropdownItem onClick={this.onClickFrom}>KAP ST. MARIE</DropdownItem>
                        <DropdownItem onClick={this.onClickFrom}>ST. HELENA</DropdownItem>
                      </DropdownMenu>
                    </ButtonDropdown>
                  </CardText>
                </CardBody>
              </Card>
              </Col>
              <Col sm="4">
              <Card>
                <CardBody>
                  <CardTitle>End Destination</CardTitle>
                  <CardText><ButtonDropdown>
                  <ButtonDropdown isOpen={this.state.dropdownTo} toggle={this.toggleTo}>
                    <DropdownToggle caret>
                      {this.state.toValue}
                    </DropdownToggle>
                      <DropdownMenu>
                        <DropdownItem onClick={this.onClickTo}>HVALBUGTEN</DropdownItem>
                        <DropdownItem onClick={this.onClickTo}>KAPSTADEN</DropdownItem>
                        <DropdownItem onClick={this.onClickTo}>MOZAMBIQUE</DropdownItem>
                        <DropdownItem onClick={this.onClickFrom}>KAP ST. MARIE</DropdownItem>
                        <DropdownItem onClick={this.onClickFrom}>ST. HELENA</DropdownItem>
                      </DropdownMenu>
                    </ButtonDropdown>
                    </ButtonDropdown></CardText>
                </CardBody>
              </Card>
              </Col>
              <Col lg="4">
              <Card>
                <CardBody>
                  <CardTitle>Weight</CardTitle>
                  <CardText>
                    <ButtonDropdown>
                    <ButtonDropdown isOpen={this.state.dropdownWeight} toggle={this.toggleWeight}>
                    <DropdownToggle caret>
                      {this.state.weightValue}
                    </DropdownToggle>
                      <DropdownMenu>
                        <DropdownItem onClick={this.onClickWeight}>less than 1 kg</DropdownItem>
                        <DropdownItem onClick={this.onClickWeight}>1-5 kg</DropdownItem>
                        <DropdownItem onClick={this.onClickWeight}>more than 5 kg </DropdownItem>
                      </DropdownMenu>
                    </ButtonDropdown>
                    </ButtonDropdown></CardText>
                </CardBody>
              </Card>
            </Col>
          </Row>
          <div class="secondRow">
          <Row>
            <div class="primSearch">
            <Card p>
                <CardBody>
                  <CardTitle>Primary Search Criteria</CardTitle>
                  <CardText>
                  <ButtonGroup>
                    <Button color="secondary" onClick={() => this.onRadioBtnClick("Minimize Delivery Time")} active={this.state.rSelected === "Minimize Delivery Time"}>Minimize Delivery Time</Button>
                    <Button color="secondary" onClick={() => this.onRadioBtnClick("Minimize Delivery Cost")} active={this.state.rSelected === "Minimize Delivery Cost"}>Minimize Delivery Cost</Button>
                  </ButtonGroup>
                  <p>Selected: {this.state.rSelected}</p>
                  </CardText>
                </CardBody>
              </Card>
            </div>
            <div class="packageType">
              <Card>
                <CardBody>
                  <CardTitle>Parcel Type </CardTitle>
                  <CardText><ButtonDropdown>
                  <ButtonDropdown isOpen={this.state.dropdownParcelType} toggle={this.toggleParcelType}>
                    <DropdownToggle caret>
                      {this.state.parcelTypeValue}
                    </DropdownToggle>
                      <DropdownMenu>
                        <DropdownItem onClick={this.onClickParcelType}>Weapons</DropdownItem>
                        <DropdownItem onClick={this.onClickParcelType}>Animals</DropdownItem>
                        <DropdownItem onClick={this.onClickParcelType}>No Special Goods</DropdownItem>
                      </DropdownMenu>
                    </ButtonDropdown>
                    </ButtonDropdown></CardText>
                </CardBody>
              </Card>
            </div> 
          </Row>
          </div>
          <Row>
          <div class="button">
          <Button color="secondary" onClick={this.onSearchClick}>Search</Button>{' '}
          </div>
          </Row>

          <Row><h3>Results Column</h3></Row>
          <Row><Col>Delivery Time {this.state.finalDuration}</Col><Col>Delivery Cost {this.state.finalPrice}</Col></Row>
        </main>
      </div>
    );
  }
}
