﻿using System;

namespace OceanicAirlinesCORE.Interfaces
{
    internal class RouteServiceException : Exception
    {
        public RouteServiceException(string error, Exception innerException) : base(error, innerException)
        {
        }
    }
}