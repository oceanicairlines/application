﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OceanicAirlinesCORE.Models
{
    public class Edge
    {
        public int id;
        public int Duration;
        public decimal Price;
        public string owner;
        public int NodeA;
        public int NodeB;

        public Edge(int id, int nodeA, int nodeB, int duration, decimal price, string owner)
        {
            this.id = id;
            this.NodeA = nodeA;
            this.NodeB = nodeB;
            this.Duration = duration;
            this.Price = price;
            this.owner = owner;
        }
    }
}
