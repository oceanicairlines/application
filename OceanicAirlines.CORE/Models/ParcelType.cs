﻿namespace OceanicAirlinesCORE.Interfaces
{
    public enum ParcelType
    {
        Weapons = 0,
        Animals =  1,
        NoSpecialGoods = 2
    }
}