﻿// ReSharper disable InconsistentNaming
namespace OceanicAirlinesCORE.Models
{
    public class ReturnDetails
    {
        public bool valid { get; set; }
        public int duration { get; set; }
        public float price { get; set; }
        public string fromName { get; set; }
        public string toName { get; set; }
    }
}
