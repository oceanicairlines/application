﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OceanicAirlinesCORE.Models
{
    public struct RouteDetails
    {
        public float Duration;
        public float Price;
        public List<Node> Path;
    }
}
