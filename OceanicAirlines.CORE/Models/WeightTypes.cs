﻿namespace OceanicAirlinesCORE.Models
{
    public enum WeightTypes
    {
        Small = 0,
        Medium = 1,
        Large = 2,
    }
}