﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OceanicAirlinesCORE.Models
{
    public class Node
    {
        public int Id;
        public string Name;
        public List<Edge> EdgeList;

        public Node(int id, string Name, List<Edge> EdgeList)
        {
            this.Id = id;
            this.Name = Name;
            this.EdgeList = EdgeList;
        }
    }
}
