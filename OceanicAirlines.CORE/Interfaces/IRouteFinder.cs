﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OceanicAirlinesCORE.Implementations;
using OceanicAirlinesCORE.Models;

namespace OceanicAirlinesCORE.Interfaces
{
    public interface IRouteFinder
    {
        RouteDetails GetPath(string start, string end, FilterTypes filter, ParcelType parcelType,
            WeightTypes weightTypes);
    }
}
