﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OceanicAirlinesCORE.Models;

namespace OceanicAirlinesCORE.Interfaces
{
    public interface IRouteService
    {
        ReturnDetails GetResult(string startNode, string endNode, WeightTypes weight, string parcelType, int filter);
    }
}
