﻿using System;
using System.Collections.Generic;
using System.Security.Policy;
using OceanicAirlinesCORE.Interfaces;
using OceanicAirlinesCORE.Models;
using System.Data.SqlClient;


namespace OceanicAirlinesCORE.Implementations
{
    public static class DataService
    {
        private static string serverName = "dbs-oadk.database.windows.net";
        private static string dbName = "db-oadk";
        private static string user = "admin-oadk";
        private static string password = "oceanicFlyAway123";
        
        public static List<Node> GetNodes(List<Edge> edgeList)
        {
            List<Node> nodes = new List<Node>();
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString =
                    "Server=" + serverName + "; Database=" + dbName + "; User Id=" + user + "; Password=" + password;
                try
                {
                    conn.Open();
                    SqlCommand command = conn.CreateCommand();
                    command.CommandText =
                        "SELECT * FROM dbo.Node";
                    command.Connection = conn;
                    SqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        List<Edge> edges = new List<Edge>();
                        var temp = reader[2].ToString();
                        if (!string.IsNullOrEmpty(temp))
                        {

                            String[] indexs = temp.Split(',');
                            foreach (String id in indexs)
                            {
                                for (int i = 0; i < edgeList.Count; i++)
                                {
                                    if (edgeList[i].id == Int32.Parse(id))
                                    {
                                        edges.Add(edgeList[i]);
                                    }
                                }
                            }
                        }
                        nodes.Add(new Node((int) reader[0], (string) reader[1], edges));
                    }


                }
                catch (SqlException e)
                {
                    Console.WriteLine("Failed fetching nodes from db: " + e);
                    return null;
                }
                conn.Close();
            }
            return nodes;
        }

        public static List<Edge> GetEdges()
        {
            List<Edge> edges = new List<Edge>();

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString =
                    "Server=" + serverName + "; Database=" + dbName + "; User Id=" + user + "; Password=" + password;
                try
                {
                    conn.Open();
                    SqlCommand command = new SqlCommand();
                    command.CommandText = "SELECT * FROM dbo.Edge";
                    command.Connection = conn;
                    SqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {                       
                        edges.Add(new Edge((int)reader[0], (int)reader[1], (int)reader[2], (int)reader[3],  (decimal)reader[4], (string)reader[5]));
                    }

                }
                catch (SqlException e)
                {
                    Console.WriteLine("Failed fetching edges from db: " + e);
                    return null;
                }
                conn.Close();
            }
            return edges;
        }
    }
}