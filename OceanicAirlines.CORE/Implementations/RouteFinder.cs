﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Runtime.InteropServices.ComTypes;
using System.Runtime.Serialization;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using OceanicAirlinesCORE.Implementations;
using OceanicAirlinesCORE.Models;

namespace OceanicAirlinesCORE.Interfaces
{
    public class RouteFinder : IRouteFinder
    {
        private List<Node> _nodeList;
        private List<Edge> _edgeList;
        private FilterTypes _filter;
        private List<List<float>> table;
        private float _priceUnit;
        private float _durationUnit;
        

        public RouteFinder()
        {
            _edgeList = DataService.GetEdges();
            _nodeList = DataService.GetNodes(_edgeList);

        }

        public RouteDetails GetPath(string start, string end, FilterTypes filter, ParcelType parcelType, WeightTypes weightTypes)
        {
            if (parcelType == ParcelType.Animals) throw new RouteServiceException("Unsupported Parcel Type", null);
            if (parcelType == ParcelType.Weapons)
            {
                _nodeList.Remove(_nodeList.Find(x => x.Name.Contains("HVALBUGTEN")));
                _nodeList.Remove(_nodeList.Find(x => x.Name.Contains("Kapstaden")));
            }
            _filter = filter;

            //Find our nearest owned node

            Node startNode;
            Node endNode;

            try
            {
                startNode = _nodeList.Find(x => x.Name.Contains(start));
                endNode = _nodeList.Find(x => x.Name.Contains(end));
            }
            catch (Exception e)
            {
                throw new RouteServiceException("Parcel type not supported at start or end node", e);
            }

            bool isOurs = false;
            foreach (Edge edge in startNode.EdgeList)
            {
                if (edge.owner == "Oceanic Airlines")
                {
                    isOurs = true;
                    break;
                }
            }

            float extraDuration = 0;
            float extraCost = 0;
            if (!isOurs)
            {
                startNode = _nodeList.Find(x => x.Name.Contains("SLAVE-KYSTEN"));
                extraCost += 10;
                extraDuration += 20;
            }

            isOurs = false;
            foreach (Edge edge in endNode.EdgeList)
            {
                if (edge.owner == "Oceanic Airlines")
                {
                    isOurs = true;
                    break;
                }
            }

            if (!isOurs)
            {
                startNode = _nodeList.Find(x => x.Name.Contains("TRIPOLI"));
                extraCost += 10;
                extraDuration += 20;
            }

            table = MakeTable();

            RouteDetails result = CalculatePath(startNode, endNode);
            result.Duration += extraDuration;
            result.Price += extraCost;

            return result;
        }

        /// <summary>
        /// This function is an implementation of Dijkstras algorithm suitable
        /// to solve the optimisation problem.
        /// The algorithm can be found in the detailed design.
        /// </summary>
        /// <param name="startNode">Node at which the algorithm should start</param>
        /// <param name="endNode">Node at which the algorithm should finish</param>
        /// <returns></returns>
        private RouteDetails CalculatePath(Node startNode, Node endNode)
        {
            var startId = startNode.Id;
            var endId = endNode.Id;

            //Create list with node id's
            var nodeSet = new List<int>();
            for (var i = 0; i < _nodeList.Count; i++) nodeSet.Add(i);

            //Initialise all weights initially to max value.
            var weight = new List<float>(_nodeList.Count);
            for (var i = 0; i < _nodeList.Count; i++) weight.Add(int.MaxValue);

            //Allows us to see what node was optimal before given node. 
            //Do not initialise, leave undefined.
            var previousNode = new List<int?>(_nodeList.Count);

            weight[startNode.Id] = 0;

            while (nodeSet.Any())
            {
                //Set current node and remove it from the node set.
                var currentNodeId = nodeSet[GetIndexOfMinimum(nodeSet)];

                nodeSet.Remove(nodeSet[currentNodeId]);
                Node currentNode = _nodeList.Find(x => x.Id.Equals(currentNodeId));

                foreach (Edge edge in currentNode.EdgeList)
                {
                    int otherNodeId = edge.NodeA == currentNode.Id ? edge.NodeB : edge.NodeA;

                    //Symmetric matrix means we can take table indices in either ORDER
                    var altWeight = weight[otherNodeId] + table[currentNodeId][otherNodeId];
                    if (altWeight < weight[otherNodeId])
                    {
                        weight[otherNodeId] = altWeight;
                        previousNode[otherNodeId] = currentNodeId;
                    }
                }

                bool isReached = true;
                for (int i = 0; i < weight.Count; i++)
                {
                    if (weight[i] < weight[endId])
                    {
                        isReached = false;
                        break;
                    }
                }

                if (isReached) break;
            }

            //Back Propogation for path
            List<Node> route = new List<Node>();

            if (previousNode[endId].HasValue)
            {
                var currentId = endId;
                while (previousNode[currentId].HasValue)
                {
                    route.Add(_nodeList.Find(x => x.Id.Equals(currentId)));
                    currentId = (int) previousNode[currentId];
                }
            }

            route.Reverse();

            return new RouteDetails
            {
                Duration = route.Count * _durationUnit,
                Price = route.Count * _priceUnit,
                Path = route,
            };
        }

        private List<List<float>> MakeTable()
        {
            List<List<float>> table = new List<List<float>>();
            foreach (Node node in _nodeList)
            {
                List<float> rowList = new List<float>();

                for (int i = 0; i < _nodeList.Count; i++) rowList.Add(0);

                foreach (Edge edge in node.EdgeList)
                {
                    int otherNodeId = edge.NodeA == node.Id ? edge.NodeB : edge.NodeA;
                    rowList[otherNodeId] = WeightFunction(edge);
                }
                table.Add(rowList);
            }

            //Make sure this table has correct number of elements.
            if (table.Count != _nodeList.Count)
                throw new RouteServiceException("Error initializing table for algorithm", null);

            foreach (List<float> list in table)
            {
                if (list.Count != _nodeList.Count)
                {
                    throw new RouteServiceException("Error initializing table for algorithm", null);
                }
            }

            return table;
        }

        private float WeightFunction(Edge edge)
        {
            //This gives preference to oceanic
            float weight = edge.owner == "Oceanic" ? 1 : 10000;

            if (_filter == FilterTypes.Duration)
            {
                weight *= edge.Duration;
            }
            else
            {
                weight *= (float) edge.Price;
            }

            return weight;
        }

        private int GetIndexOfMinimum(List<int> list)
        {
            int index = 0;
            float currentValue = list[0];
            for (int i = 0; i < list.Count; i++)
            {
                if (list[i] > currentValue)
                {
                    currentValue = list[i];
                    index = i;
                }
            }

            return index;
        }
    }
}