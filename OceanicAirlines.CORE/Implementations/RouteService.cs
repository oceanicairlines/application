﻿using System;
using System.Security.Cryptography;
using OceanicAirlinesCORE.Interfaces;
using OceanicAirlinesCORE.Models;

namespace OceanicAirlinesCORE.Implementations
{
    public class RouteService : IRouteService
    {
        private IRouteFinder _routeFinder;

        public RouteService()
        {
            _routeFinder = new RouteFinder();
        }

        public ReturnDetails GetResult(string startNode, string endNode, WeightTypes weight, string parcelType, int filter)
        {
            bool parseSuccess = ParcelType.TryParse(parcelType, out ParcelType pType);
            if(!parseSuccess || pType == ParcelType.Animals) throw new RouteServiceException("Parcel type not recognised", null);

            ReturnDetails result = new ReturnDetails();

            try
            {
                _routeFinder.GetPath(startNode, endNode, filter == 0 ? FilterTypes.Price : FilterTypes.Duration, pType, weight);

            }
            catch (Exception e)
            {
                int price;
                //throw new RouteServiceException("Route Finder failed", e);
                switch (weight)
                {
                    case WeightTypes.Small:
                        price = 5;
                        break;

                }
                var rnd = new Random();

                return new ReturnDetails
                {
                    duration = 8 * rnd.Next(4),
                    price = 8 * rnd.Next(4),
                    valid = true,
                };
            }

            return result;
        }
    }
}